#! /bin/bash
echo " starting the script"
var=(2 4 6 1 6 7)
avg () {
     if [ $# -le 1 ]; then
        echo " Need atleast 2 values"
        return
     fi
     sum=0
     count=0
     for num in $*
     do
        sum=`expr $sum + $num`
        count=`expr $count + 1`
         
     done
	echo " sum= $sum"
        echo " Count= $count"
        avg=`expr $sum / $count`
        return $avg
}

# invoke your function
avg ${var[*]}
ret=$?
echo " Avergae is $ret"
echo " --------------------"
avg 1 2 3 4
# capture value returned by last command
ret=$?
echo " Avergae is $ret"
