#! /bin/bash

usage () {
  echo " Usage: $0 <arg1> <agr2>....... <agr n> "
}

if [ $# -le 1 ]; then
   echo " Please enter 2 numbers"
   usage
   exit
fi
sum=0
count=0
for i in $*
do
    sum=`expr $sum + $i`
    count=`expr $count + 1`
done
    echo "Sum= $sum"
    echo "count= $count"
    avg=`expr $sum / $count`
    echo "avg= $avg"
