#!/bin/bash
var1=10
var2=0
echo "var1= $var1"
echo "var2= $var2"

echo " --- true-----"
if [ $var1 ]; then
	echo " 1. - Got a true value "
fi

echo "----- equals to ------"
if [ $var1 == $var2 ]; then
	echo "2.- var1 is equal to var2 "
fi

echo "----- not equals to ------"
if [ $var1 != $var2 ]; then
	echo "3-  var1 is not equal to var2 "
fi

echo " end of script"
